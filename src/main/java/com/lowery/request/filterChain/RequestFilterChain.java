package com.lowery.request.filterChain;

import com.lowery.request.Request;
import com.lowery.request.RequestContext;

import java.util.List;

/**
 * Created by cong.yu on 16/9/18.
 */
public interface RequestFilterChain<P extends Request,R> {
    Entry<P, R> getEntry(String name);

    Entry<P, R> getEntry(RequestFilter<P, R> filter);

    Entry<P, R> getEntry(Class<? extends RequestFilter<P, R>> filterType);

    RequestFilter<P, R> get(String name);

    RequestFilter<P, R> get(Class<? extends RequestFilter<P, R>> filterType);

    RequestFilter.NextFilter<P, R> getNextFilter(String name);

    RequestFilter.NextFilter<P, R> getNextFilter(RequestFilter<P, R> filter);

    RequestFilter.NextFilter<P, R> getNextFilter(Class<? extends RequestFilter<P, R>> filterType);

    List<Entry<P, R>> getAll();

    List<Entry<P, R>> getAllReversed();

    boolean contains(String name);

    boolean contains(RequestFilter<P, R> filter);

    boolean contains(Class<? extends RequestFilter<P, R>> filterType);

    void addFirst(String name, RequestFilter<P, R> filter);

    void addLast(String name, RequestFilter<P, R> filter);

    void addBefore(String baseName, String name, RequestFilter<P, R> filter);

    void addAfter(String baseName, String name, RequestFilter<P, R> filter);

    RequestFilter<P, R> replace(String name, RequestFilter<P, R> newFilter);

    void replace(RequestFilter<P, R> oldFilter, RequestFilter<P, R> newFilter);

    RequestFilter<P, R> replace(Class<? extends RequestFilter<P, R>> oldFilterType, RequestFilter<P, R> newFilter);

    RequestFilter<P, R> remove(String name);

    void remove(RequestFilter<P, R> filter);

    RequestFilter<P, R> remove(Class<? extends RequestFilter<P, R>> filterType);

    void clear() throws Exception;

    public R fireRequest(RequestContext context, P request) throws Exception;

    public interface Entry<P extends Request, R> {

        /**
         * Returns the name of the filter.
         */
        String getName();

        /**
         * Returns the filter.
         */
        RequestFilter<P, R> getFilter();

        /**
         * @return The {@link RequestFilter.NextFilter} of the filter.
         */
        RequestFilter.NextFilter<P, R> getNextFilter();

        /**
         * Adds the specified filter with the specified name just before this entry.
         */
        void addBefore(String name, RequestFilter<P, R> filter);

        /**
         * Adds the specified filter with the specified name just after this entry.
         */
        void addAfter(String name, RequestFilter<P, R> filter);

        /**
         * Replace the filter of this entry with the specified new filter.
         */
        void replace(RequestFilter<P, R> newFilter);

        /**
         * Removes this entry from the chain it belongs to.
         */
        void remove();
    }
}
