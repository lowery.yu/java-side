package com.lowery.springframework.JdbcTemplate.entity;

/**
 * Created by cong.yu on 16/9/15.
 */
public class Customer {

    private Long id;

    private String firstName;

    private String lastName;

    public Customer(Long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
