package com.lowery.request.filterChain;

import com.lowery.request.Request;
import com.lowery.request.RequestContext;

/**
 * Created by cong.yu on 16/9/18.
 */
public interface RequestFilter<P extends Request,R> {
    R request(RequestContext context, NextFilter<P,R> nextFilter, P request) throws Exception;

    interface NextFilter<P extends Request, R> {
        R request(RequestContext context, P request) throws Exception;
    }
}
