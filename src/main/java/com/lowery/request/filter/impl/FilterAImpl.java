package com.lowery.request.filter.impl;

import com.lowery.request.BasicRequest;
import com.lowery.request.Request;
import com.lowery.request.RequestContext;
import com.lowery.request.filterChain.RequestFilterAdapter;

/**
 * Created by cong.yu on 16/9/18.
 */
public class FilterAImpl<R> extends RequestFilterAdapter<BasicRequest,R> {

    @Override
    public Object request(RequestContext context, NextFilter nextFilter, Request request)
            throws Exception {

        System.out.println("doing filter a impl.........");
        R r = (R)super.request(context, nextFilter, request);
        System.out.println("after filter a.........");
        return r;
    }
}
