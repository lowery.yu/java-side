package com.lowery.request;

import java.util.List;
import java.util.Set;

/**
 * Created by cong.yu on 16/9/18.
 */
public class BasicRequest implements Request {

    private String       originalSql;
    private String       interfaceName;
    private List<Object> originalParams;
    private Set<String> dbNameSet;

    public String getOriginalSql() {
        return originalSql;
    }

    public void setOriginalSql(String originalSql) {
        this.originalSql = originalSql;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public List<Object> getOriginalParams() {
        return originalParams;
    }

    public void setOriginalParams(List<Object> originalParams) {
        this.originalParams = originalParams;
    }

    public Set<String> getDbNameSet() {
        return dbNameSet;
    }

    public void setDbNameSet(Set<String> dbNameSet) {
        this.dbNameSet = dbNameSet;
    }
}
