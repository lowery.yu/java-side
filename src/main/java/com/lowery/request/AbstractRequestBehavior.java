package com.lowery.request;

/**
 * Created by cong.yu on 16/9/18.
 */
public abstract class AbstractRequestBehavior<P extends Request, R> implements RequestBehavior<P,R>{

    protected RequestHandler<P, R> handler;

    /**
     * @return the handler
     */
    public RequestHandler<P, R> getHandler() {
        return handler;
    }

    /**
     * @param handler the handler to set
     */
    public void setHandler(RequestHandler<P, R> handler) {
        this.handler = handler;
    }
}
