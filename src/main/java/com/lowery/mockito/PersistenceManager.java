package com.lowery.mockito;

import java.io.File;

/**
 * Created by cong.yu on 16/9/9.
 */
public class PersistenceManager {

    public boolean createDirectoryStructure(String directoryPath) {
        File directory = new File(directoryPath);

        if (directory.exists()) {
            throw new IllegalArgumentException("\"" + directoryPath + "\" already exists.");
        }

        return directory.mkdirs();
    }
}
