package com.lowery.lambda;

import com.google.common.collect.Lists;
import com.lowery.lambda.entity.Person;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by cong.yu on 16/9/12.
 */
public class PersonComparatorExample {

    public static void main(String[] args) {
        List<Person> persons = initListData();

        Collections.sort(persons, Comparator.comparing(Person::getFirstName).thenComparing(Person::getLastName)
                .thenComparing(Person::getAge));

        System.out.println(persons);
    }

    private static List<Person> initListData() {
        List<Person> persons = Lists.newArrayList(
                new Person("Allen", "Smith", 31),
                new Person("Ted", "Neward", 42),
                new Person("Charlotte", "Neward", 39),
                new Person("Michael", "Neward", 19),
                new Person("Matthew", "Neward", 13),
                new Person("Neal", "Ford", 45),
                new Person("Candy", "Ford", 39),
                new Person("Jeff", "Brown", 43),
                new Person("Betsy", "Brown", 39));
        return persons;
    }
}
