package com.lowery.springframework.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringBootComponentscanApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootComponentscanApplication.class, args);
	}
}
