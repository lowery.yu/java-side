package com.lowery.request.filterChain;

import com.lowery.request.Request;
import com.lowery.request.RequestBehavior;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.lowery.request.RequestContext;
import com.lowery.request.filterChain.RequestFilter.NextFilter;
/**
 * Created by cong.yu on 16/9/18.
 */
public class DefaultRequestFilterChain<P extends Request,R> implements RequestFilterChain {

    private final Map<String, Entry<P, R>> name2entry = new ConcurrentHashMap<String, Entry<P, R>>();

    /** The chain head */
    private final EntryImpl                head;

    /** The chain tail */
    private final EntryImpl                tail;

    /** current service */
    private RequestBehavior<P, R> requestBehavior;

    public DefaultRequestFilterChain(RequestBehavior<P, R> requestBehavior){
        if (requestBehavior == null) {
            throw new NullPointerException("requestBehavior is  null");
        }
        this.requestBehavior = requestBehavior;
        head = new EntryImpl(null, null, "head", new HeadFilter());
        tail = new EntryImpl(head, null, "tail", new TailFilter());
        head.nextEntry = tail;
    }

    public Entry<P, R> getEntry(String name) {
        Entry<P, R> e = name2entry.get(name);
        if (e == null) {
            return null;
        }
        return e;
    }

    public Entry<P, R> getEntry(RequestFilter filter) {
        EntryImpl e = head.nextEntry;
        while (e != tail) {
            if (e.getFilter() == filter) {
                return e;
            }
            e = e.nextEntry;
        }
        return null;
    }


    public Entry<P, R> getEntry(Class filterType) {
        EntryImpl e = head.nextEntry;
        while (e != tail) {
            if (filterType.isAssignableFrom(e.getFilter().getClass())) {
                return e;
            }
            e = e.nextEntry;
        }
        return null;
    }

    public RequestFilter<P, R> get(String name) {
        Entry<P, R> e = getEntry(name);
        if (e == null) {
            return null;
        }

        return e.getFilter();
    }



    public RequestFilter<P, R> get(Class filterType) {
        Entry<P, R> e = getEntry(filterType);
        if (e == null) {
            return null;
        }

        return e.getFilter();
    }

    public NextFilter<P, R> getNextFilter(String name) {
        Entry<P, R> e = getEntry(name);
        if (e == null) {
            return null;
        }

        return e.getNextFilter();
    }

    public NextFilter<P, R> getNextFilter(RequestFilter filter) {
        Entry<P, R> e = getEntry(filter);
        if (e == null) {
            return null;
        }

        return e.getNextFilter();
    }

    @Override
    public NextFilter<P, R> getNextFilter(Class filterType) {
        Entry<P, R> e = getEntry(filterType);
        if (e == null) {
            return null;
        }

        return e.getNextFilter();
    }

    public synchronized void addFirst(String name, RequestFilter filter) {
        checkAddable(name);
        register(head, name, filter);
    }

    public synchronized void addLast(String name, RequestFilter filter) {
        checkAddable(name);
        register(tail.prevEntry, name, filter);
    }

    public synchronized void addBefore(String baseName, String name, RequestFilter filter) {
        EntryImpl baseEntry = checkOldName(baseName);
        checkAddable(name);
        register(baseEntry.prevEntry, name, filter);
    }

    public synchronized void addAfter(String baseName, String name, RequestFilter filter) {
        EntryImpl baseEntry = checkOldName(baseName);
        checkAddable(name);
        register(baseEntry, name, filter);
    }

    public synchronized RequestFilter<P, R> remove(String name) {
        EntryImpl entry = checkOldName(name);
        deregister(entry);
        return entry.getFilter();
    }

    @Override
    public synchronized void remove(RequestFilter filter) {
        EntryImpl e = head.nextEntry;
        while (e != tail) {
            if (e.getFilter() == filter) {
                deregister(e);
                return;
            }
            e = e.nextEntry;
        }
        throw new IllegalArgumentException("Filter not found: " + filter.getClass().getName());
    }

    public synchronized RequestFilter<P, R> remove(Class filterType) {
        EntryImpl e = head.nextEntry;
        while (e != tail) {
            if (filterType.isAssignableFrom(e.getFilter().getClass())) {
                RequestFilter<P, R> oldFilter = e.getFilter();
                deregister(e);
                return oldFilter;
            }
            e = e.nextEntry;
        }
        throw new IllegalArgumentException("Filter not found: " + filterType.getName());
    }

    public synchronized RequestFilter<P, R> replace(String name, RequestFilter newFilter) {
        EntryImpl entry = checkOldName(name);
        RequestFilter<P, R> oldFilter = entry.getFilter();
        entry.setFilter(newFilter);
        return oldFilter;
    }

    public synchronized void replace(RequestFilter oldFilter, RequestFilter newFilter) {
        EntryImpl e = head.nextEntry;
        while (e != tail) {
            if (e.getFilter() == oldFilter) {
                e.setFilter(newFilter);
                return;
            }
            e = e.nextEntry;
        }
        throw new IllegalArgumentException("Filter not found: " + oldFilter.getClass().getName());
    }

    public synchronized RequestFilter<P, R> replace(Class oldFilterType,
                                                    RequestFilter newFilter) {
        EntryImpl e = head.nextEntry;
        while (e != tail) {
            if (oldFilterType.isAssignableFrom(e.getFilter().getClass())) {
                RequestFilter<P, R> oldFilter = e.getFilter();
                e.setFilter(newFilter);
                return oldFilter;
            }
            e = e.nextEntry;
        }
        throw new IllegalArgumentException("Filter not found: " + oldFilterType.getName());
    }

    public synchronized void clear() throws Exception {
        List<Entry<P, R>> l = new ArrayList<Entry<P, R>>(name2entry.values());
        for (RequestFilterChain.Entry<P, R> entry : l) {
            try {
                deregister((EntryImpl) entry);
            } catch (Exception e) {
                throw new RequestFilterLifeCycleException("clear(): " + entry.getName(), e);
            }
        }
    }

    private void register(EntryImpl prevEntry, String name, RequestFilter<P, R> filter) {
        EntryImpl newEntry = new EntryImpl(prevEntry, prevEntry.nextEntry, name, filter);

        prevEntry.nextEntry.prevEntry = newEntry;
        prevEntry.nextEntry = newEntry;
        name2entry.put(name, newEntry);
    }

    private void deregister(EntryImpl entry) {

        deregister0(entry);
    }

    private void deregister0(EntryImpl entry) {
        EntryImpl prevEntry = entry.prevEntry;
        EntryImpl nextEntry = entry.nextEntry;
        prevEntry.nextEntry = nextEntry;
        nextEntry.prevEntry = prevEntry;

        name2entry.remove(entry.name);
    }

    /**
     * Throws an exception when the specified filter name is not registered in this chain.
     *
     * @return An filter entry with the specified name.
     */
    private EntryImpl checkOldName(String baseName) {
        EntryImpl e = (EntryImpl) name2entry.get(baseName);
        if (e == null) {
            throw new IllegalArgumentException("Filter not found:" + baseName);
        }
        return e;
    }

    /**
     * Checks the specified filter name is already taken and throws an exception if already taken.
     */
    private void checkAddable(String name) {
        if (name2entry.containsKey(name)) {
            throw new IllegalArgumentException("Other filter is using the same name '" + name + "'");
        }
    }

    public R fireRequest(RequestContext context, Request request) throws Exception {

        Entry<P, R> head = this.head;
        return callNextOnQuery(context, head, (P)request);
    }

    private R callNextOnQuery(RequestContext context, Entry<P, R> entry, P request) throws Exception {
        try {
            RequestFilter<P, R> filter = entry.getFilter();
            NextFilter<P, R> nextFilter = entry.getNextFilter();
            return filter.request(context, nextFilter, request);
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Entry<P, R>> getAll() {
        List<Entry<P, R>> list = new ArrayList<Entry<P, R>>();
        EntryImpl e = head.nextEntry;
        while (e != tail) {
            list.add(e);
            e = e.nextEntry;
        }

        return list;
    }

    public List<Entry<P, R>> getAllReversed() {
        List<Entry<P, R>> list = new ArrayList<Entry<P, R>>();
        EntryImpl e = tail.prevEntry;
        while (e != head) {
            list.add(e);
            e = e.prevEntry;
        }
        return list;
    }

    public boolean contains(String name) {
        return getEntry(name) != null;
    }

    public boolean contains(RequestFilter filter) {
        return getEntry(filter) != null;
    }

    public boolean contains(Class filterType) {
        return getEntry(filterType) != null;
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append("{ ");

        boolean empty = true;

        EntryImpl e = head.nextEntry;
        while (e != tail) {
            if (!empty) {
                buf.append(", ");
            } else {
                empty = false;
            }

            buf.append('(');
            buf.append(e.getName());
            buf.append(':');
            buf.append(e.getFilter());
            buf.append(')');

            e = e.nextEntry;
        }

        if (empty) {
            buf.append("empty");
        }

        buf.append(" }");

        return buf.toString();
    }

    private class HeadFilter extends RequestFilterAdapter<P, R> {
    }

    private class TailFilter extends RequestFilterAdapter<P, R> {
        @Override
        public Object request(RequestContext context, NextFilter nextFilter, Request request)
                throws Exception {
            return requestBehavior.getHandler().request(context, (P) request);
        }
    }

    private class EntryImpl implements Entry<P, R> {

        private EntryImpl              prevEntry;

        private EntryImpl              nextEntry;

        private final String           name;

        private RequestFilter<P, R>    filter;

        private final NextFilter<P, R> nextFilter;

        private EntryImpl(EntryImpl prevEntry, EntryImpl nextEntry, String name, RequestFilter<P, R> filter){
            if (filter == null) {
                throw new IllegalArgumentException("filter");
            }
            if (name == null) {
                throw new IllegalArgumentException("name");
            }

            this.prevEntry = prevEntry;
            this.nextEntry = nextEntry;
            this.name = name;
            this.filter = filter;
            this.nextFilter = new NextFilter<P, R>() {

                public R request(RequestContext context, P request) throws Exception {
                    Entry<P, R> nextEntry = EntryImpl.this.nextEntry;
                    return callNextOnQuery(context, nextEntry, request);
                }

                public String toString() {
                    return EntryImpl.this.nextEntry.name;
                }
            };
        }

        public String getName() {
            return name;
        }

        public RequestFilter<P, R> getFilter() {
            return filter;
        }

        private void setFilter(RequestFilter<P, R> filter) {
            if (filter == null) {
                throw new IllegalArgumentException("filter");
            }

            this.filter = filter;
        }

        public NextFilter<P, R> getNextFilter() {
            return nextFilter;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();

            // Add the current filter
            sb.append("('").append(getName()).append('\'');

            // Add the previous filter
            sb.append(", prev: '");

            if (prevEntry != null) {
                sb.append(prevEntry.name);
                sb.append(':');
                sb.append(prevEntry.getFilter().getClass().getSimpleName());
            } else {
                sb.append("null");
            }

            // Add the next filter
            sb.append("', next: '");

            if (nextEntry != null) {
                sb.append(nextEntry.name);
                sb.append(':');
                sb.append(nextEntry.getFilter().getClass().getSimpleName());
            } else {
                sb.append("null");
            }

            sb.append("')");
            return sb.toString();
        }

        public void addAfter(String name, RequestFilter<P, R> filter) {
            DefaultRequestFilterChain.this.addAfter(getName(), name, filter);
        }

        public void addBefore(String name, RequestFilter<P, R> filter) {
            DefaultRequestFilterChain.this.addBefore(getName(), name, filter);
        }

        public void remove() {
            DefaultRequestFilterChain.this.remove(getName());
        }

        public void replace(RequestFilter<P, R> newFilter) {
            DefaultRequestFilterChain.this.replace(getName(), newFilter);
        }
    }
}
