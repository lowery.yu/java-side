package com.lowery.request.filterChain;

import com.lowery.request.Request;

/**
 * Created by cong.yu on 16/9/18.
 */
public interface FilterChainBuilder<P extends Request, R> {

    void buildFilterChain(RequestFilterChain<P, R> chain) throws Exception;

}
