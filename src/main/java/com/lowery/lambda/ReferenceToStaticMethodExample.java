package com.lowery.lambda;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by cong.yu on 16/9/12.
 */
public class ReferenceToStaticMethodExample {

    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        Predicate predicate = ReferenceToStaticMethodExample::isPrime;

//        Predicate predicate= (number) -> ReferenceToStaticMethodExample.isPrime(number);

        List<Integer> primeNumbers = ReferenceToStaticMethodExample.findPrimeNumbers(numbers,predicate);

        System.out.println("prime numbers = " + primeNumbers);
    }

    //判断一个元素是否是质数
    private static boolean isPrime(Object number) {
        if ((int)number == 1) {
            return false;
        }
        for (int i = 2; i < (int)number; i++) {
            if ((int)number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static List<Integer> findPrimeNumbers(List<Integer> numbers, Predicate predicate) {
        List<Integer> sortedNumber = Lists.newArrayList();
        numbers.stream().filter((number) -> predicate.test(number))
                .forEach((number) -> sortedNumber.add(number));
        return sortedNumber;
    }

}
