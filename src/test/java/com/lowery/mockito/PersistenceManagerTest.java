package com.lowery.mockito;

import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;

import static org.junit.Assert.*;

/**
 * Created by cong.yu on 16/9/9.
 */
@RunWith(PowerMockRunner.class)
//@RunWith(SpringJUnit4ClassRunner.class)
@PrepareForTest(PersistenceManager.class)
public class PersistenceManagerTest {

    @Test
    public void testCreateDirectoryConstructure_ok() throws Exception {
        final String path = "directoryPath";

        File fileMock = PowerMock.createMock(File.class);
        PersistenceManager tested = new PersistenceManager();
        PowerMock.expectNew(File.class, path).andReturn(fileMock);

        EasyMock.expect(fileMock.exists()).andReturn(false);
        EasyMock.expect(fileMock.mkdirs()).andReturn(true);

        PowerMock.replayAll(fileMock, File.class);

        assertTrue(tested.createDirectoryStructure(path));

        PowerMock.verifyAll();
    }
}
