package com.lowery.request.provider;

import com.lowery.request.AbstractRequestBehavior;
import com.lowery.request.BasicRequest;
import com.lowery.request.Request;
import com.lowery.request.RequestContext;
import com.lowery.request.filter.impl.FilterAImpl;
import com.lowery.request.filter.impl.FilterBImpl;
import com.lowery.request.filter.impl.FilterCImpl;
import com.lowery.request.filterChain.DefaultRequestFilterChain;
import com.lowery.request.filterChain.DefaultRequestFilterChainBuilder;
import com.lowery.request.filterChain.RequestFilterChain;
import com.lowery.request.handler.RequestHandlerImpl;
import org.easymock.internal.Results;

/**
 * Created by cong.yu on 16/9/18.
 */
public class ProviderTest extends AbstractRequestBehavior {

    public final RequestFilterChain<BasicRequest, Results> filterChain = new DefaultRequestFilterChain<BasicRequest, Results>(this);

    public void initData() throws Exception {
        DefaultRequestFilterChainBuilder<BasicRequest, Results> builder = new DefaultRequestFilterChainBuilder();
        builder.addLast("implea",new FilterAImpl());
        builder.addLast("impleb",new FilterBImpl());
        builder.addLast("implec",new FilterCImpl());

        builder.buildFilterChain(filterChain);
    }

    public static void main(String[] args) throws Exception {
        ProviderTest test = new ProviderTest();
        test.setHandler(new RequestHandlerImpl());
        Results results = (Results) test.execute(null,null);
        System.out.println(results);
    }

    @Override
    public Object execute(RequestContext context, Request param) throws Exception {
        initData();

        Results results = filterChain.fireRequest(new RequestContext(), new BasicRequest());

        return results;
    }
}
