package com.lowery.mockito.entity;

/**
 * Created by cong.yu on 16/9/9.
 */
public class Message {

    private final long id;
    private final String content;

    public Message(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
