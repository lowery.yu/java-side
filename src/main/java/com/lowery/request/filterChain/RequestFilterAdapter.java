package com.lowery.request.filterChain;

import com.lowery.request.Request;
import com.lowery.request.RequestContext;

/**
 * Created by cong.yu on 16/9/18.
 */
public class RequestFilterAdapter<P extends Request, R> implements RequestFilter {
    @Override
    public Object request(RequestContext context, NextFilter nextFilter, Request request)
            throws Exception {
        return nextFilter.request(context, request);
    }

    public String toString() {
        return this.getClass().getSimpleName();
    }

}
