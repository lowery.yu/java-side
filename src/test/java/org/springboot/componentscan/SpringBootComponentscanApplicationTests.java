package org.springboot.componentscan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootComponentscanApplicationTests.class)
public class SpringBootComponentscanApplicationTests {

	@Test
	public void contextLoads() {
		System.out.println("test..... ");
	}

}
