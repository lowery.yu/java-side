package com.lowery.request;

/**
 * Created by cong.yu on 16/9/18.
 */
public interface RequestHandler<P extends Request, R> {
    R request(RequestContext context, P request) throws Exception;
}
