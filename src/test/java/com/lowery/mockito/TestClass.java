package com.lowery.mockito;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by cong.yu on 16/9/10.
 */
public class TestClass {

    @Test
    public void testObject(){
        List<Object> lists= Lists.newArrayList();
        Map<String,String> maps= null;

        for(int i=0;i<10;i++){
            maps=Maps.newHashMap();
            maps.put(i+"",i+"");
            lists.add(maps);
        }
        System.out.println(lists);
    }

    @Test
    public void testabc(){
        List<Integer> numbers=new ArrayList<>(13);
        numbers.forEach(System.out::println);

        for(int i=0;i<12;i++){
            numbers.add(i);
        }

        System.out.println(numbers);

    }
}
