package com.lowery.springframework.rest;

import com.google.common.collect.Lists;
import com.lowery.springframework.rest.entity.Greeting;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by cong.yu on 16/9/8.
 */
@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {

        List<Integer> lists= Lists.newArrayList();
        Random random=new Random();
        for (int i=0;i<1000000;i++){
            lists.add(random.nextInt(1000000));
        }
        long startTime = System.currentTimeMillis();

        Optional<Integer> max = lists.stream().max(Comparator.naturalOrder());
        System.out.println("max="+max.get());

        System.out.println("spent time = "+(System.currentTimeMillis()-startTime));

        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }

}
