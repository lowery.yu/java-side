package com.lowery.request.filterChain;

import com.lowery.request.Request;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CopyOnWriteArrayList;
import com.lowery.request.filterChain.RequestFilterChain.Entry;

/**
 * Created by cong.yu on 16/9/18.
 */
public class DefaultRequestFilterChainBuilder<P extends Request,R> implements FilterChainBuilder{

    private final List<Entry<P, R>> entries;

    public DefaultRequestFilterChainBuilder(){
        entries = new CopyOnWriteArrayList<RequestFilterChain.Entry<P, R>>();
    }

    /**
     * @see RequestFilterChain#getEntry(String)
     */
    public RequestFilterChain.Entry<P, R> getEntry(String name) {
        for (RequestFilterChain.Entry<P, R> e : entries) {
            if (e.getName().equals(name)) {
                return e;
            }
        }

        return null;
    }

    /**
     * @see RequestFilterChain#getEntry(RequestFilter)
     */
    public Entry<P, R> getEntry(RequestFilter<P, R> filter) {
        for (Entry<P, R> e : entries) {
            if (e.getFilter() == filter) {
                return e;
            }
        }

        return null;
    }

    /**
     * @see RequestFilterChain#getEntry(Class)
     */
    public Entry<P, R> getEntry(Class<? extends RequestFilter<P, R>> filterType) {
        for (Entry<P, R> e : entries) {
            if (filterType.isAssignableFrom(e.getFilter().getClass())) {
                return e;
            }
        }

        return null;
    }

    /**
     * @see RequestFilterChain#get(String)
     */
    public RequestFilter<P, R> get(String name) {
        Entry<P, R> e = getEntry(name);
        if (e == null) {
            return null;
        }

        return e.getFilter();
    }

    /**
     * @see RequestFilterChain#get(Class)
     */
    public RequestFilter<P, R> get(Class<? extends RequestFilter<P, R>> filterType) {
        Entry<P, R> e = getEntry(filterType);
        if (e == null) {
            return null;
        }

        return e.getFilter();
    }

    /**
     * @see RequestFilterChain#getAll()
     */
    public List<Entry<P, R>> getAll() {
        return new ArrayList<Entry<P, R>>(entries);
    }

    /**
     * @see RequestFilterChain#getAllReversed()
     */
    public List<Entry<P, R>> getAllReversed() {
        List<Entry<P, R>> result = getAll();
        Collections.reverse(result);
        return result;
    }

    /**
     * @see RequestFilterChain#contains(String)
     */
    public boolean contains(String name) {
        return getEntry(name) != null;
    }

    /**
     * @see RequestFilterChain#contains(RequestFilter)
     */
    public boolean contains(RequestFilter<P, R> filter) {
        return getEntry(filter) != null;
    }

    /**
     * @see RequestFilterChain#contains(Class)
     */
    public boolean contains(Class<? extends RequestFilter<P, R>> filterType) {
        return getEntry(filterType) != null;
    }

    /**
     * @see RequestFilterChain#addFirst(String, RequestFilter)
     */
    public synchronized void addFirst(String name, RequestFilter<P, R> filter) {
        register(0, new EntryImpl(name, filter));
    }

    /**
     * @see RequestFilterChain#addLast(String, RequestFilter)
     */
    public synchronized void addLast(String name, RequestFilter<P, R> filter) {
        register(entries.size(), new EntryImpl(name, filter));
    }

    /**
     * @see RequestFilterChain#addBefore(String, String, RequestFilter)
     */
    public synchronized void addBefore(String baseName, String name, RequestFilter<P, R> filter) {
        checkBaseName(baseName);

        for (ListIterator<Entry<P, R>> i = entries.listIterator(); i.hasNext();) {
            Entry<P, R> base = i.next();
            if (base.getName().equals(baseName)) {
                register(i.previousIndex(), new EntryImpl(name, filter));
                break;
            }
        }
    }

    /**
     * @see RequestFilterChain#addAfter(String, String, RequestFilter)
     */
    public synchronized void addAfter(String baseName, String name, RequestFilter<P, R> filter) {
        checkBaseName(baseName);

        for (ListIterator<Entry<P, R>> i = entries.listIterator(); i.hasNext();) {
            Entry<P, R> base = i.next();
            if (base.getName().equals(baseName)) {
                register(i.nextIndex(), new EntryImpl(name, filter));
                break;
            }
        }
    }

    /**
     * @see RequestFilterChain#remove(String)
     */
    public synchronized RequestFilter<P, R> remove(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name");
        }

        for (ListIterator<Entry<P, R>> i = entries.listIterator(); i.hasNext();) {
            Entry<P, R> e = i.next();
            if (e.getName().equals(name)) {
                entries.remove(i.previousIndex());
                return e.getFilter();
            }
        }

        throw new IllegalArgumentException("Unknown filter name: " + name);
    }

    /**
     * @see RequestFilterChain#remove(RequestFilter)
     */
    public synchronized RequestFilter<P, R> remove(RequestFilter<P, R> filter) {
        if (filter == null) {
            throw new IllegalArgumentException("filter");
        }

        for (ListIterator<Entry<P, R>> i = entries.listIterator(); i.hasNext();) {
            Entry<P, R> e = i.next();
            if (e.getFilter() == filter) {
                entries.remove(i.previousIndex());
                return e.getFilter();
            }
        }

        throw new IllegalArgumentException("Filter not found: " + filter.getClass().getName());
    }

    /**
     * @see RequestFilterChain#remove(Class)
     */
    public synchronized RequestFilter<P, R> remove(Class<? extends RequestFilter<P, R>> filterType) {
        if (filterType == null) {
            throw new IllegalArgumentException("filterType");
        }

        for (ListIterator<Entry<P, R>> i = entries.listIterator(); i.hasNext();) {
            Entry<P, R> e = i.next();
            if (filterType.isAssignableFrom(e.getFilter().getClass())) {
                entries.remove(i.previousIndex());
                return e.getFilter();
            }
        }

        throw new IllegalArgumentException("Filter not found: " + filterType.getName());
    }

    public synchronized RequestFilter<P, R> replace(String name, RequestFilter<P, R> newFilter) {
        checkBaseName(name);
        EntryImpl e = (EntryImpl) getEntry(name);
        RequestFilter<P, R> oldFilter = e.getFilter();
        e.setFilter(newFilter);
        return oldFilter;
    }

    public synchronized void replace(RequestFilter<P, R> oldFilter, RequestFilter<P, R> newFilter) {
        for (Entry<P, R> e : entries) {
            if (e.getFilter() == oldFilter) {
                ((EntryImpl) e).setFilter(newFilter);
                return;
            }
        }
        throw new IllegalArgumentException("Filter not found: " + oldFilter.getClass().getName());
    }

    public synchronized void replace(Class<? extends RequestFilter<P, R>> oldFilterType, RequestFilter<P, R> newFilter) {
        for (Entry<P, R> e : entries) {
            if (oldFilterType.isAssignableFrom(e.getFilter().getClass())) {
                ((EntryImpl) e).setFilter(newFilter);
                return;
            }
        }
        throw new IllegalArgumentException("Filter not found: " + oldFilterType.getName());
    }

    /**
     * @see RequestFilterChain#clear()
     */
    public synchronized void clear() {
        entries.clear();
    }

    @Override
    public void buildFilterChain(RequestFilterChain chain) throws Exception {
        for (Entry<P, R> e : entries) {
            chain.addLast(e.getName(), e.getFilter());
        }
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append("{ ");

        boolean empty = true;

        for (Entry<P, R> e : entries) {
            if (!empty) {
                buf.append(", ");
            } else {
                empty = false;
            }

            buf.append('(');
            buf.append(e.getName());
            buf.append(':');
            buf.append(e.getFilter());
            buf.append(')');
        }

        if (empty) {
            buf.append("empty");
        }

        buf.append(" }");

        return buf.toString();
    }

    private void checkBaseName(String baseName) {
        if (baseName == null) {
            throw new IllegalArgumentException("baseName");
        }

        if (!contains(baseName)) {
            throw new IllegalArgumentException("Unknown filter name: " + baseName);
        }
    }

    private void register(int index, Entry<P, R> e) {
        if (contains(e.getName())) {
            throw new IllegalArgumentException("Other filter is using the same name: " + e.getName());
        }

        entries.add(index, e);
    }

    private class EntryImpl implements RequestFilterChain.Entry<P, R> {

        private final String                 name;

        private volatile RequestFilter<P, R> filter;

        private EntryImpl(String name, RequestFilter<P, R> filter){
            if (name == null) {
                throw new IllegalArgumentException("name");
            }
            if (filter == null) {
                throw new IllegalArgumentException("filter");
            }

            this.name = name;
            this.filter = filter;
        }

        public String getName() {
            return name;
        }

        public RequestFilter<P, R> getFilter() {
            return filter;
        }

        private void setFilter(RequestFilter<P, R> filter) {
            this.filter = filter;
        }

        public RequestFilter.NextFilter<P, R> getNextFilter() {
            throw new IllegalStateException();
        }

        @Override
        public String toString() {
            return "(" + getName() + ':' + filter + ')';
        }

        public void addAfter(String name, RequestFilter<P, R> filter) {
            DefaultRequestFilterChainBuilder.this.addAfter(getName(), name, filter);
        }

        public void addBefore(String name, RequestFilter<P, R> filter) {
            DefaultRequestFilterChainBuilder.this.addBefore(getName(), name, filter);
        }

        public void remove() {
            DefaultRequestFilterChainBuilder.this.remove(getName());
        }

        public void replace(RequestFilter<P, R> newFilter) {
            DefaultRequestFilterChainBuilder.this.replace(getName(), newFilter);
        }
    }

}
