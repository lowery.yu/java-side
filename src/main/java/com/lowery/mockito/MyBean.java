package com.lowery.mockito;

import com.lowery.mockito.entity.Message;
import org.springframework.stereotype.Component;

/**
 * Created by cong.yu on 16/9/9.
 */
@Component
public class MyBean {
    public Message generateMessage() {
        final long id = IdGenerator.generateNewId();
        return new Message(id, "My bean message");
    }
}
