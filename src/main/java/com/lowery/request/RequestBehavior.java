package com.lowery.request;

/**
 * Created by cong.yu on 16/9/18.
 */
public interface RequestBehavior<P extends Request, R> {
    void setHandler(RequestHandler<P, R> handler);

    RequestHandler<P, R> getHandler();

    R execute(RequestContext context, P param) throws Exception;
}
