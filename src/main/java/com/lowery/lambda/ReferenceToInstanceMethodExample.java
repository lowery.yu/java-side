package com.lowery.lambda;

import java.util.Arrays;

/**
 * Created by cong.yu on 16/9/12.
 */
public class ReferenceToInstanceMethodExample {

    public static void main(String[] args) {
        String[] stringArray = { "Barbara", "James", "Mary", "John", "Patricia", "Robert", "Michael", "Linda" };
        Arrays.sort(stringArray, String::compareToIgnoreCase);
        System.out.println("after sorted:" + Arrays.toString(stringArray));
    }
}
