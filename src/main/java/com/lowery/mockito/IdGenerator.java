package com.lowery.mockito;

/**
 * Created by cong.yu on 16/9/9.
 */
public class IdGenerator {
    public static long generateNewId() {
        return System.currentTimeMillis();
    }
}
