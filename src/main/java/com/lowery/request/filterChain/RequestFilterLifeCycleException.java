package com.lowery.request.filterChain;

/**
 *
 * @author <a href="http://mina.apache.org">Apache MINA Project</a>
 */
public class RequestFilterLifeCycleException extends RuntimeException {

    private static final long serialVersionUID = -5542098881633506449L;

    public RequestFilterLifeCycleException(){
    }

    public RequestFilterLifeCycleException(String message){
        super(message);
    }

    public RequestFilterLifeCycleException(String message, Throwable cause){
        super(message, cause);
    }

    public RequestFilterLifeCycleException(Throwable cause){
        super(cause);
    }
}
